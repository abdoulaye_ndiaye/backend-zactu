$(document)
    .ready(
    function (e) {
        initPageElements();
        function getPost(page) {
        $(".imload").fadeIn("1000");

            $( "#" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
            $(".imload").fadeIn("1000");
            $('#tbodyPosts').html('');
            $('#pagingTop').html('');
            $('#pagingBottom').html('');
            $('#divPagination').empty();
            $('#divPagination').removeData("twbs-pagination");
            $('#divPagination').unbind("page");
            appRoutes.controllers.PostController.getAllPosts(page).ajax({
                success: function (data) {
                    console.log(data);
                    if (data.code == 200) {
                        if(data.posts.length != 0){
                            var posts = data.posts;
                            var numLine = (data.per_page * data.current_page) - data.per_page;
                            if (data.current_page == -1){numLine = 0}
                            for (var i in posts) {

                                var html = '';
                                numLine += 1;
                                html += '<tr">';
                                html += '<td>' + numLine + '</td>';
                                html += '<td>' + posts[i].id + '</td>';
                                html += '<td>' + posts[i].authorid + '</td>';
                                html += '<td>' + posts[i].author + '</td>';
                                html += '<td>' + posts[i].photouser + '</td>';
                                html += '<td>' + posts[i].url + '</td>';
                                html += '<td>' + posts[i].category + '</td>';
                                html += '<td>' + posts[i].sexe + '</td>';
                                html += '<td>' + posts[i].more + '</td>';
                                html += '<td>' + posts[i].telephone + '</td>';
                                html += '<td>' + posts[i].pays + '</td>';
                                html += '<td>' + posts[i].date + '</td>';
                                html += '<td><a id ="' + posts[i].id + '-' + posts[i].authorid + '-' + posts[i].author + '"href="#" class="btn btn-danger btn-xs delete-post">Supprimer</a></td>';
                                if(posts[i].etat == '0'){
                                    html += '<td><a id ="' + posts[i].id + '-' + posts[i].authorid + '-' + posts[i].author + '"href="#" class="btn btn-success btn-xs validate-post">Valider</a></td>';
                                }
                                html += '<td><a id ="' + posts[i].id + '-' + posts[i].authorid + '-' + posts[i].author + '"href="#" class="btn btn-warning btn-xs update-post">Update</a></td>';

                                html += '</tr>';
                                $('#tbodyPosts').append(html);

                            }

                            var current_page = data.current_page;
                            if(current_page == -1){current_page = 1}
                            $('#pagingTop').append(current_page + '/' + data.total_page + ' pages');
                            $('#pagingBottom').append(current_page + '/' + data.total_page + ' pages');
                            $('#divPaginationhaut').twbsPagination({
                                totalPages: data.total_page,
                                visiblePages: 3,
                                startPage: page,
                                onPageClick: function (event, numPage) {
                                    page = numPage;
                                    getPost(page);
                                }
                            });
                            $('#divPagination').twbsPagination({
                                totalPages: data.total_page,
                                visiblePages: 3,
                                startPage: page,
                                onPageClick: function (event, numPage2) {
                                    page = numPage2;
                                    getPost(page);
                                }
                            });
                        }else{
                            $('#voucherVide').html("Aucun voucher vendu");
                            $('#zoneRecherche').hide();
                        }

                    } else if (data.result == "nok") {
                        alert(data.message);
                    }

                    $(".imload").fadeOut("1000");

                    $('.delete-post').click(function () {
                       var idd = $(this).attr('id');
                       console.log(idd);
                       var spp = idd.split('-');
                       var idPost = spp[0];
                       deletePost(idPost);
                    });

                    $('.validate-post').click(function () {
                       var idd = $(this).attr('id');
                       console.log(idd);
                       var spp = idd.split('-');
                       var idPost = spp[0];
                       validatePost(idPost);
                    });
                },
                error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                    if (objetExeption == "Unauthorized") {
                        $(location).attr('href', "/");
                    }
                    $(".imload").fadeOut("1000");
                }
            });
        }

        function initPageElements() {

            $("#typefacture").fadeOut();
            $("#m_reglement").addClass("active");
            $("#filtre_date").val("");

            getPost();

        }

        function showDateField() {
            $('#filtre_date').show();
            $('#filtre_date_2').show();
            $('#filtre_operande').hide();
        }

        function hideDateField() {
            $('#filtre_date').hide();
            $('#filtre_date_2').hide();
            $('#filtre_operande').show();
        }

        function showTheFiltreField(idField) {
            $('#filtre_text').val("");
            $('#filtre_text').hide();
            $('#filtre_etat').hide();
            $('#filtre_date').hide();
            hideDateField();

            if(idField == "filtre_date"){
                showDateField();
            } else {
                $('#' + idField).show();
            }
        }

        function getReglementAfterSeach(data, page) {
            $(".imload").fadeIn("1000");
            $( "#" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
            $(".imload").fadeIn("1000");
            $('#tbodyReglement').html('');
            $('#pagingTop').html('');
            $('#pagingBottom').html('');
            $('#divPagination').empty();
            $('#divPagination').removeData("twbs-pagination");
            $('#divPagination').unbind("page");
            appRoutes.controllers.YatooListController.getListAfterSeach(page).ajax({
            data : JSON.stringify(data),
            contentType : 'application/json',
            success : function (data){
            console.log(data)
                if (data.result == "ok") {
                    var vouchers = data.vouchers;

                    var numLine = (data.per_page * data.current_page) - data.per_page;
                    if (data.current_page == -1){numLine = 0}
                    $('#nbr_ligne_tableau').html(data.total +" ligne(s) trouvee(s)");
                    for (var i in vouchers) {

                        var html = '';
                        numLine += 1;
                        html += '<tr">';
                        html += '<td>' + numLine + '</td>';
                        html += '<td>' + vouchers[i].id + '</td>';
                        html += '<td>' + vouchers[i].pin + '</td>';
                        html += '<td>' + vouchers[i].nom + '</td>';
                        html += '<td>' + vouchers[i].prenom + '</td>';
                        html += '<td>' + vouchers[i].mobile + '</td>';
                        html += '<td>' + vouchers[i].etat + '</td>';
                        html += '<td>' + vouchers[i].datechargement + '</td>';
                        html += '<td>' + vouchers[i].dateachat + '</td>';
                        html += '<td><a id ="' + vouchers[i].id + '-' + vouchers[i].pin + '-' + vouchers[i].mobile + '"href="#" class="btn btn-danger btn-xs send-msg">Renvoyer Code</a></td>';
                        html += '</tr>';

                        $('#tbodyReglement').append(html);
                    }

                    var current_page = data.current_page;
                    if(current_page == -1){current_page = 1}
                    $('#pagingTop').append(current_page + '/' + data.total_page + ' pages');
                    $('#pagingBottom').append(current_page + '/' + data.total_page + ' pages');
                    $('#divPaginationhaut').twbsPagination({
                        totalPages: data.total_page,
                        visiblePages: 3,
                        startPage: page,
                        onPageClick: function (event, numPage) {
                            page = numPage;
                            getReglementAfterSeach(data, page);
                        }
                    });
                    $('#divPagination').twbsPagination({
                        totalPages: data.total_page,
                        visiblePages: 3,
                        startPage: page,
                        onPageClick: function (event, numPage2) {
                            page = numPage2;
                            getReglementAfterSeach(data, page);
                        }
                    });

                } else if (data.result == "nok") {
                    alert(data.message);
                }

                $(".imload").fadeOut("1000");

                $('.delete-post').click(function () {
                   var idd = $(this).attr('id');
                   console.log(idd);
                   var spp = idd.split('-');
                   var idPost = spp[0];
                   deletePost(idPost);
                });
                $('.validate-post').click(function () {
                   var idd = $(this).attr('id');
                   console.log(idd);
                   var spp = idd.split('-');
                   var idPost = spp[0];
                   validatePost(idPost);
                });
            },
            error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                if (objetExeption == "Unauthorized") {
                    $(location).attr('href', "/");
                }
                $(".imload").fadeOut("1000");
            }
          });
        }

        function deletePost(data) {
            $(".imload").fadeIn("1000");
            appRoutes.controllers.PostController.deletePost(data).ajax({
            data : JSON.stringify(data),
            contentType : 'application/json',
            success : function (data){
            console.log(data)
                if (data.code == 200) {
                    alert(data.message);
                    getPost();
                } else {
                    alert(data.message);
                }

                $(".imload").fadeOut("1000");
            },
            error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                if (objetExeption == "Unauthorized") {
                    $(location).attr('href', "/");
                }
                $(".imload").fadeOut("1000");
            }
          });
        }

        function validatePost(data) {
            $(".imload").fadeIn("1000");
            appRoutes.controllers.PostController.validatePost(data).ajax({
            data : JSON.stringify(data),
            contentType : 'application/json',
            success : function (data){
            console.log(data)
                if (data.code == 200) {
                    alert(data.message);
                    getPost();
                } else {
                    alert(data.message);
                }

                $(".imload").fadeOut("1000");
            },
            error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                if (objetExeption == "Unauthorized") {
                    $(location).attr('href', "/");
                }
                $(".imload").fadeOut("1000");
            }
          });
        }

        $("#bt_reche_reglement").click(function () {
           var idTransaction = $('#idTransaction').val();
           var idVoucher = $('#idVoucher').val();
           var jsonObj = {
                      "idtransaction": idTransaction,
                      "idvoucher":idVoucher
                     }

           getReglementAfterSeach(jsonObj);
        });

        $('p').on('click', '.del-element', function() {
            var text = $(this).text();
            var id = text.toLowerCase();
            id = id.replace(/ /g,"");
            $('#'+ id +'').fadeIn();
            $(this).next().remove();
            $(this).next().remove();
            $(this).fadeOut();
        });

        $('.del-sodeci').click(function(){
           $('#typefacture').fadeIn();
           $(this).next().remove();
           $(this).next().remove();
           $(this).fadeOut();
        })

});

