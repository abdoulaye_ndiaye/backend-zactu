package tools;

import play.Logger;
import play.db.DB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by ghambyte on 2/17/16.
 */
public class DBUtils {

    public static ResultSet doRequest(String req) throws SQLException {
        Connection c = DB.getConnection();
        Statement statement;
        try {
            statement = c.createStatement();
            return statement.executeQuery(req);
        } catch (SQLException e) {
            Logger.error("doRequest SQLException " + e.getMessage());
            return null;
        } finally {
            Db.closeQuietly(c);
        }
    }

    public static boolean checkConfirmation(String codeConfirmaiton, String imei) {
        String req = "select * from session where imei='" + imei + "' AND code_validation='"
                + codeConfirmaiton + "'";

        Connection c = DB.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = c.createStatement();
            resultSet = statement.executeQuery(req);
            return resultSet != null && resultSet.next();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c, statement, resultSet);
        }
    }

    public static boolean checkSession(String codeClient, String imei) {
        String req = "select * from session where code_collecteur='" + codeClient + "' AND imei='"
                + imei + "'";

        Logger.debug(req);
        Connection c = DB.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = c.createStatement();
            resultSet = statement.executeQuery(req);
            return resultSet != null && resultSet.next();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c, statement, resultSet);
        }
    }

    public static boolean checkSession(String codeClient) {
        String req = "select * from session where code_client='" + codeClient + "'";

        Logger.debug(req);
        Connection c = DB.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = c.createStatement();
            resultSet = statement.executeQuery(req);
            return resultSet != null && resultSet.next();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c, statement, resultSet);
        }
    }

    public static String getIdCollecteur(String codeClient) {
        String req = "select id from collecteur where matricule='" + codeClient + "'";

        Logger.debug(req);
        Connection c = DB.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = c.createStatement();
            resultSet = statement.executeQuery(req);
            if (resultSet == null) {
                return null;
            }
            if (resultSet.next()) {
                String idCollecteur = resultSet.getString("id");
                return idCollecteur;
            }
            return null;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return null;
        } finally {
            Db.closeQuietly(c, statement, resultSet);
        }
    }

    public static boolean checkDevice(String imei) {
        String req = "select * from session where imei='" + imei + "' AND status='online'";

        Connection c = DB.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = c.createStatement();
            resultSet = statement.executeQuery(req);
            return resultSet != null && resultSet.next();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c, statement, resultSet);
        }
    }

    public static ArrayList<String> checkStatus(String imei) {
        String req = "SELECT * FROM session as s, collecteur as c, structures as st WHERE s.imei='"+imei+"' AND s.code_collecteur=c.matricule AND c.ce_structure=st.id_structure AND s.status='online' limit 1";
        //"SELECT * FROM session as s, collecteur as c, structures as st WHERE s.imei='"+imei+"' AND s.code_collecteur=c.matricule AND c.ce_structure=st.id_structure"

        Logger.debug(req);
        Connection c = DB.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = c.createStatement();
            resultSet = statement.executeQuery(req);
            if (resultSet == null) {
                return null;
            }
            if (resultSet.next()) {
                String codeClient = resultSet.getString("code_collecteur");
                String nom = resultSet.getString("nom");
                String prenom = resultSet.getString("prenom");
                String libelle = resultSet.getString("libelle");
                String groupe = "UNACOOPEC";
                ArrayList<String> strings = new ArrayList<>();
                strings.add(codeClient);
                strings.add(nom);
                strings.add(prenom);
                strings.add(libelle);
                strings.add(groupe);
                return strings;
            }
            return null;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return null;
        } finally {
            Db.closeQuietly(c, statement, resultSet);
        }
    }

    public static ArrayList<String> checkconfirmation(String imei) {
        String req = "select s.code_client, a.nomcompletabonne as nom from session as s "
                + "inner join abonne as a on a.codeabonne = s.code_client "
                + "where s.imei='" + imei + "' limit 1";

        Logger.info("checkconfirmation >> " + req);
        Connection c = DB.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = c.createStatement();
            resultSet = statement.executeQuery(req);
            if (resultSet == null) {
                return null;
            }
            if (resultSet.next()) {
                String codeClient = resultSet.getString("code_client");
                String nom = resultSet.getString("nom");
                ArrayList<String> strings = new ArrayList<>();
                strings.add(codeClient);
                strings.add(nom);
                return strings;
            }
            return null;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return null;
        } finally {
            Db.closeQuietly(c, statement, resultSet);
        }
    }


    public static void updateSession(String imei) {
        String req = "update session set code_validation='online' where imei='" + imei + "'";
        Logger.info(req);
        Connection c = DB.getConnection();
        Statement statement = null;
        try {
            statement = c.createStatement();
            statement.executeUpdate(req);
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        } finally {
            Db.closeQuietly(c);
            Db.closeQuietly(statement);
        }
    }

    public static boolean disconnectImei(String imei) {
        String req = "delete from session where imei='" + imei + "'";
        Logger.info(req);
        Connection c = DB.getConnection();
        Statement statement = null;
        int resultSet;
        try {
            statement = c.createStatement();
            resultSet = statement.executeUpdate(req);
            return resultSet == 1;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c);
            Db.closeQuietly(statement);
        }
    }

    public static boolean disconnectAbonne(String codeClient) {
        String req = "delete from session where code_client='" + codeClient + "'";
        Logger.info(req);
        Connection c = DB.getConnection();
        Statement statement = null;
        int resultSet;
        try {
            statement = c.createStatement();
            resultSet = statement.executeUpdate(req);
            return resultSet == 1;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c);
            Db.closeQuietly(statement);
        }

    }
}
