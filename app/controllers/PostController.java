package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import dao.DAOFactory;
import dao.PostDAO;
import io.swagger.annotations.*;
import models.PostModel;
import play.Logger;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import tools.Log;
import tools.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static tools.Const.DEFAULT_PER_PAGE;
import static tools.Utils.getObjectNode;
import static tools.Utils.getTotalRows;

/**
 * Created by mac on 19/09/2016.
 */
@Api(value = "/lesposts", description = "Renvoie les posts")
public class PostController extends Controller {
    @ApiOperation(
            nickname = "/lesposts",
            value = "List post",
            httpMethod = "GET",
            consumes = ("application/json"),
            produces = ("application/json"),
            response = PostModel.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Posts recuperes.", response = PostModel.class),
            @ApiResponse(code = 450, message = "Parametres incorrects.", response = PostModel.class),
            @ApiResponse(code = 451, message = "Donnees erronees.", response = PostModel.class),
    })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "page", value = "page courante", required = false, dataType = "int", paramType = "query")

    })
    public F.Promise<Result> getPosts(String authorId, String category, String sexe, int page, int per_page) {

        return F.Promise.promise(() -> {
            Logger.debug("THE PAGE ### " + page);
            PostDAO dao = DAOFactory.getPostDAO();
            ObjectNode result = Json.newObject();
            ArrayList<PostModel> list = dao.getListPosts(authorId, category, sexe, page, DEFAULT_PER_PAGE);

            if (list == null) {
                ObjectNode node = Utils.getObjectNode(451, "Aucun post trouve");
                Log.logActionOutput(node.toString());
                return status(451, node);
            }
            String totalRequest = "SELECT count(*) as total from posts";
            int total = getTotalRows(totalRequest);
            int totalPage = total / per_page;
            if (total % per_page > 0) {
                totalPage++;
            }
            result.put("total", total);
            result.put("total_page", totalPage);
            result.put("current_page", page);
            result.put("per_page", per_page);
            result.put("result", "ok");
            result.put("code", 200);
            result.set("posts", Json.toJson(list));
            Log.logActionOutput(result.toString());
            return ok(result);

        });
    }

    @ApiOperation(
            nickname = "/getPostById",
            value = "Détail du post",
            httpMethod = "GET",
            consumes = ("application/json"),
            produces = ("application/json"),
            response = PostModel.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Post recupere.", response = PostModel.class),
            @ApiResponse(code = 450, message = "Parametres incorrects.", response = PostModel.class),
            @ApiResponse(code = 451, message = "Donnees erronees.", response = PostModel.class),
    })
    public F.Promise<play.mvc.Result> getPostById(String id) {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            ObjectNode result = Json.newObject();
            PostModel postModel = dao.getPost(id);

            if (postModel == null) {
                ObjectNode node = Utils.getObjectNode(451, "Aucun post trouve");
                Log.logActionOutput(node.toString());
                return status(451, node);
            }

            result.put("code", 200);
            result.set("post", Json.toJson(postModel));
            Log.logActionOutput(result.toString());
            return ok(result);

        });
    }

    @ApiOperation(
            nickname = "/dopost",
            value = "Faire un post",
            httpMethod = "POST",
            consumes = ("application/json"),
            produces = ("application/json"),
            response = PostModel.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Post effectue.", response = PostModel.class),
            @ApiResponse(code = 450, message = "Parametres incorrects.", response = PostModel.class),
            @ApiResponse(code = 451, message = "Donnees erronees.", response = PostModel.class),
    })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(value = "Parametres du post", required = true, dataType = "models.PostModel", paramType = "body")
    })
    public F.Promise<play.mvc.Result> doPost() {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            Logger.info("JSON REQ: " + request().body());
            JsonNode json = request().body().asJson();
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            try {
                String author = json.findPath("author").textValue();
                String authorId = json.findPath("authorid").textValue();
                String url = json.findPath("url").textValue();
                String category = json.findPath("category").textValue();
                String sexe = json.findPath("sexe").textValue();
                String more = json.findPath("more").textValue();
                String photouser = json.findPath("photouser").textValue();
                String pays = json.findPath("pays").textValue();
                String datepost = dateFormat.format(date);
                String telephone = json.findPath("telephone").textValue();
                String etat = json.findPath("etat").textValue();
                String[] data = new String[]{author};
                if (!Utils.checkData(data)) {
                    return ok(Utils.getObjectNode(450, "Parametres incorrects."));
                }
                PostModel postModel = new PostModel(author, url, category, sexe, authorId, more, photouser, pays, datepost, telephone, etat);
                boolean postModels = dao.doPost(postModel);
                if (!postModels) {
                    ObjectNode node = getObjectNode(452, "Aucun post");
                    Log.logActionOutput(node.toString());
                    return status(452, node);
                }

                ObjectNode node = getObjectNode(200, "Post bien enregistre.");
                Log.logActionOutput(node.toString());
                return ok(node);

            } catch (NullPointerException e) {
                Logger.error(e.getMessage());
                return ok(Utils.getObjectNode(450, "Parametres incorrects."));
            }

        });
    }

    @ApiOperation(
            nickname = "/delepost",
            value = "Supprimer un post",
            httpMethod = "POST",
            consumes = ("application/json"),
            produces = ("application/json"),
            response = PostModel.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Post supprime.", response = PostModel.class),
            @ApiResponse(code = 450, message = "Parametres incorrects.", response = PostModel.class),
            @ApiResponse(code = 451, message = "Donnees erronees.", response = PostModel.class),
    })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(value = "Parametres du post", required = true, dataType = "String", paramType = "body")
    })
    public F.Promise<play.mvc.Result> deletePost(String id) {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            Logger.info("JSON REQ: " + request().body());
            JsonNode json = request().body().asJson();
            try {
                String[] data = new String[]{id};
                if (!Utils.checkData(data)) {
                    return ok(Utils.getObjectNode(450, "Parametres incorrects."));
                }
                boolean deletedPost = dao.deletePost(id);
                if (!deletedPost) {
                    ObjectNode node = getObjectNode(452, "Aucun post");
                    Log.logActionOutput(node.toString());
                    return status(452, node);
                }

                ObjectNode node = getObjectNode(200, "Post Supprime.");
                Log.logActionOutput(node.toString());
                return ok(node);

            } catch (NullPointerException e) {
                Logger.error(e.getMessage());
                return ok(Utils.getObjectNode(450, "Parametres incorrects."));
            }

        });
    }

    @ApiOperation(
            nickname = "/updatepost",
            value = "Mettre A jour un post",
            httpMethod = "POST",
            consumes = ("application/json"),
            produces = ("application/json"),
            response = PostModel.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Post mis A jour.", response = PostModel.class),
            @ApiResponse(code = 450, message = "Parametres incorrects.", response = PostModel.class),
            @ApiResponse(code = 451, message = "Donnees erronees.", response = PostModel.class),
    })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(value = "Parametres du post", required = true, dataType = "models.PostModel", paramType = "body")
    })
    public F.Promise<play.mvc.Result> updatePost(String id) {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            Logger.info("JSON REQ: " + request().body());
            JsonNode json = request().body().asJson();
            try {
                String author = json.findPath("author").textValue();
                String authorId = json.findPath("authorid").textValue();
                String url = json.findPath("url").textValue();
                String category = json.findPath("category").textValue();
                String sexe = json.findPath("sexe").textValue();
                String more = json.findPath("more").textValue();
                String photouser = json.findPath("photouser").textValue();
                String pays = json.findPath("pays").textValue();
                String date = json.findPath("date").textValue();
                String telephone = json.findPath("telephone").textValue();
                String etat = json.findPath("etat").textValue();
                String[] data = new String[]{id};
                if (!Utils.checkData(data)) {
                    return ok(Utils.getObjectNode(450, "Parametres incorrects."));
                }
                PostModel postModel = new PostModel(author, url, category, sexe, authorId, more, photouser, pays, date, telephone, etat);
                boolean deletedPost = dao.updatePost(id, postModel);
                if (!deletedPost) {
                    ObjectNode node = getObjectNode(452, "Aucun post");
                    Log.logActionOutput(node.toString());
                    return status(452, node);
                }

                ObjectNode node = getObjectNode(200, "Post Mis à jour.");
                node.put("result", "ok");
                Log.logActionOutput(node.toString());
                return ok(node);

            } catch (NullPointerException e) {
                Logger.error(e.getMessage());
                return ok(Utils.getObjectNode(450, "Parametres incorrects."));
            }

        });
    }

    public F.Promise<Result> getAllPosts(int page, int per_page) {

        return F.Promise.promise(() -> {
            Logger.debug("THE PAGE ### " + page);
            PostDAO dao = DAOFactory.getPostDAO();
            ObjectNode result = Json.newObject();
            ArrayList<PostModel> list = dao.getAllPosts(page, DEFAULT_PER_PAGE, false);

            if (list == null) {
                ObjectNode node = Utils.getObjectNode(451, "Aucun post trouve");
                Log.logActionOutput(node.toString());
                return status(451, node);
            }
            String totalRequest = "SELECT count(*) as total from posts";
            int total = getTotalRows(totalRequest);
            int totalPage = total / per_page;
            if (total % per_page > 0) {
                totalPage++;
            }
            result.put("total", total);
            result.put("total_page", totalPage);
            result.put("current_page", page);
            result.put("per_page", per_page);
            result.put("result", "ok");
            result.put("code", 200);
            result.set("posts", Json.toJson(list));
            Log.logActionOutput(result.toString());
            return ok(result);

        });
    }

    public F.Promise<play.mvc.Result> validatePost(String id) {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            Logger.info("JSON REQ: " + request().body());
            JsonNode json = request().body().asJson();
            try {
                String[] data = new String[]{id};
                if (!Utils.checkData(data)) {
                    return ok(Utils.getObjectNode(450, "Parametres incorrects."));
                }
                boolean deletedPost = dao.validatePost(id);
                if (!deletedPost) {
                    ObjectNode node = getObjectNode(452, "Aucun post");
                    Log.logActionOutput(node.toString());
                    return status(452, node);
                }

                ObjectNode node = getObjectNode(200, "Post Validé.");
                Log.logActionOutput(node.toString());
                return ok(node);

            } catch (NullPointerException e) {
                Logger.error(e.getMessage());
                return ok(Utils.getObjectNode(450, "Parametres incorrects."));
            }

        });
    }

}
