package models;

import java.util.List;

/**
 * Created by mac on 18/09/2016.
 */
public class PostModel {

    private String id;
    private String url;
    private String author;
    private String category;
    private String sexe;
    private String authorid;
    private String more;
    private String photouser;
    private String pays;
    private String date;
    private String telephone;
    private String texte;
    private String titre;
    private String posttitre;
    private String posttexte;
    private String datepost;
    private String etat;
    List<String> urls;
    List<VenteModel> ventes;

    public PostModel() {
    }

    public PostModel(String category, String texte, String titre, List<String> urls, List<VenteModel> ventes) {
        this.category = category;
        this.texte = texte;
        this.titre = titre;
        this.urls = urls;
        this.ventes = ventes;
    }

    public PostModel(String url, String author, String category, String sexe, String authorid, String more,
                     String photouser, String pays, String date, String telephone, String etat) {
        this.url = url;
        this.author = author;
        this.category = category;
        this.sexe = sexe;
        this.authorid = authorid;
        this.more = more;
        this.photouser = photouser;
        this.pays = pays;
        this.date = date;
        this.telephone = telephone;
        this.etat = etat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getAuthorid() {
        return authorid;
    }

    public void setAuthorid(String authorid) {
        this.authorid = authorid;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhotouser() {
        return photouser;
    }

    public void setPhotouser(String photouser) {
        this.photouser = photouser;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public String getPosttitre() {
        return posttitre;
    }

    public void setPosttitre(String posttitre) {
        this.posttitre = posttitre;
    }

    public String getPosttexte() {
        return posttexte;
    }

    public void setPosttexte(String posttexte) {
        this.posttexte = posttexte;
    }

    public String getDatepost() {
        return datepost;
    }

    public void setDatepost(String datepost) {
        this.datepost = datepost;
    }

    public List<VenteModel> getVentes() {
        return ventes;
    }

    public void setVentes(List<VenteModel> ventes) {
        this.ventes = ventes;
    }

    @Override
    public String toString() {
        return "PostModel{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", author='" + author + '\'' +
                ", category='" + category + '\'' +
                ", sexe='" + sexe + '\'' +
                ", authorid='" + authorid + '\'' +
                ", more='" + more + '\'' +
                ", photouser='" + photouser + '\'' +
                ", pays='" + pays + '\'' +
                ", date='" + date + '\'' +
                ", telephone='" + telephone + '\'' +
                ", texte='" + texte + '\'' +
                ", titre='" + titre + '\'' +
                ", posttitre='" + posttitre + '\'' +
                ", posttexte='" + posttexte + '\'' +
                ", datepost='" + datepost + '\'' +
                ", etat='" + etat + '\'' +
                ", urls=" + urls +
                ", ventes=" + ventes +
                '}';
    }
}
