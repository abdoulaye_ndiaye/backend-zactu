package models;

/**
 * Created by Oumou on 11/05/2016.
 */
public class Recherche {

    private  String idtransaction;

    private  String idvoucher;

    public Recherche(String idtransaction, String idvoucher) {
        this.idtransaction = idtransaction;
        this.idvoucher = idvoucher;
    }

    public String getIdtransaction() {
        return idtransaction;
    }

    public void setIdtransaction(String idtransaction) {
        this.idtransaction = idtransaction;
    }

    public String getIdvoucher() {
        return idvoucher;
    }

    public void setIdvoucher(String idvoucher) {
        this.idvoucher = idvoucher;
    }
}
