package dao;

import models.PostModel;
import java.util.ArrayList;

/**
 * Created by ghambyte on 9/3/16.
 */
public interface PostDAO {

    String AUTHOR = "author";

    String AUTHORID = "authorid";

    String ID = "id";

    String URL = "url";

    String MORE = "more";

    String PHOTOUSER = "photouser";

    String DATEPOST = "datepost";

    String CATEGORY = "category";

    String SEXE = "sexe";

    String PAYS = "pays";

    String TELEPHONE = "telephone";

    String ETAT = "etat";

    String POSTID = "postid";
    String POSTITRE = "posttitre";
    String POSTTEXTE = "posttexte";

    ArrayList<PostModel> getListPosts(String authorId, String category, String sexe, int page, int per_page);

    ArrayList<PostModel> getAllPosts(int page, int per_page, boolean all);

    PostModel getPost(String id);

    boolean doPost(PostModel postModel);

    boolean deletePost(String id);

    boolean validatePost(String id);

    boolean updatePost(String id, PostModel postModel);

}
